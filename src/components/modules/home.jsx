import ent1 from '../../assets/images/articles/ent1.jpg'
export const Home = () => {
    return <section>
        <div class="container">
            <h1 class="editor-h1">Trending</h1>
            <div class="articles">
                <a href="./html/article.html" class="card">
                    <img src={ent1}  alt="" />
                    <article>
                        <p class="entertainment-category">Entertainment</p>
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi
                            nisi neque eum libero maiores voluptatem repudiandae quos
                            perspiciatis, reiciendis dolor!
                        </p>
                    </article>
                </a>

                <a href="" class="card">
                <img src="https://images.pexels.com/photos/51953/mother-daughter-love-sunset-51953.jpeg?auto=compress&cs=tinysrgb&w=600"  alt="" height={150} />
                    <article>
                        <p class="sports-category">Sports</p>
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi
                            nisi neque eum libero maiores voluptatem repudiandae quos
                            perspiciatis, reiciendis dolor!
                        </p>
                    </article>
                </a>

                <a href="./html/article.html" class="card">
                    <img src="https://images.pexels.com/photos/20222375/pexels-photo-20222375/free-photo-of-scruffy-dog-in-meadow.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load" alt="" />
                    <article>
                        <p class="technology-category">Technology</p>
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi
                            nisi neque eum libero maiores voluptatem repudiandae quos
                            perspiciatis, reiciendis dolor!
                        </p>
                    </article>
                </a>

                <a href="./html/article.html" class="card">
                    <article>
                        <p class="sports-category">Sports</p>
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi
                            nisi neque eum libero maiores voluptatem repudiandae quos
                            perspiciatis, reiciendis dolor!
                        </p>
                    </article>
                    <img src="https://images.pexels.com/photos/2127733/pexels-photo-2127733.jpeg?auto=compress&cs=tinysrgb&w=600" alt="" />
                </a>

                <a href="./html/article.html" class="card">
                    <img src="https://images.pexels.com/photos/13861/IMG_3496bfree.jpg?auto=compress&cs=tinysrgb&w=600" alt="" />
                    <article>
                        <p class="technology-category">Technology</p>
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi
                            nisi neque eum libero maiores voluptatem repudiandae quos
                            perspiciatis, reiciendis dolor!
                        </p>
                    </article>
                </a>

                <a href="./html/article.html" class="card">
                <img src="https://images.pexels.com/photos/4709289/pexels-photo-4709289.jpeg?auto=compress&cs=tinysrgb&w=600"  alt="" height={150} />

                    <article>
                        <p class="sports-category">Sports</p>
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi
                            nisi neque eum libero maiores voluptatem repudiandae quos
                            perspiciatis, reiciendis dolor!
                        </p>
                    </article>
                </a>

                <a href="./html/article.html" class="card">
                    <article>
                        <p class="entertainment-category">Entertainment</p>
                        <h1>Lorem ipsum dolor sit amet.</h1>
                        <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sequi
                            nisi neque eum libero maiores voluptatem repudiandae quos
                            perspiciatis, reiciendis dolor!
                        </p>
                    </article>
                    <img src="https://images.pexels.com/photos/57645/pexels-photo-57645.jpeg?auto=compress&cs=tinysrgb&w=600" alt="" />
                </a>
            </div>
        </div>
    </section>
    

}