import { Link ,NavLink  } from "react-router-dom";
export const Navbar = () => {
    return <nav>
    <div class="container">
        <div class="logo">
            <i class="fas fa-globe fa-3x"></i>
            <h1>News<span>KTM</span></h1>
        </div>
        <div class="options">
            <NavLink to="/home">Home</NavLink>
            <NavLink to="/about">About</NavLink>
            

            {/* <a href="" class="current">Home</a>
            <a href="" class="current">Home</a> */}
           
        </div>
    </div>
</nav>
}