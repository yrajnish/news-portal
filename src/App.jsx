
import './App.css'
import { Route, Routes } from "react-router";
import { Navbar } from "./components/partials/Navbar";
import { Footer } from "./components/partials/Footer";
import { Home } from './components/modules/home';
import { About } from './components/modules/about';
export const App = () => { 
  return <>
    <Navbar />
    <Routes>
    <Route path='/home' element={<Home/>} />
      <Route path='/about' element={<About/>}/>
    </Routes>
    <Footer />
  </>
}

